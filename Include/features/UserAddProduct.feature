@UserAddProduct @Smoke
Feature: UserAddProduct
  As a user, I want to add product in secondhand-store website

  @TC-WEB-PROD-004.001
  Scenario: User Add Product with the Valid Information
    Then User click Icon Catalogs Valid Data
    Then User click Tambah Produk Valid Data
    Then User input Nama Produk Valid Data
    Then User input Harga Produk Valid Data
    Then User select option Kategori Valid Data
    Then User input deskripsi Produk Valid Data
    Then User click button terbitkan Valid Data

  @TC-WEB-PROD-004.002
  Scenario: User Add Product with the Invalid Format
    Then User click Icon Catalogs Invalid Format
    Then User click Tambah Produk Invalid Format
    Then User input Nama Produk Invalid Format
    Then User input Harga Produk Invalid Format
    Then User select option Kategori Invalid Format
    Then User input deskripsi Produk Invalid Format
    Then User click button terbitkan Invalid Format

  @TC-WEB-PROD-004.003
  Scenario: User Add Product with the Missed Information
    Then User click Icon Catalogs Missed Information
    Then User click Tambah Produk Missed Information
    Then User input Nama Produk Missed Information
    Then User input Harga Produk Missed Information
    Then User select option Kategori Missed Information
    Then User input deskripsi Produk Missed Information
    Then User click button terbitkan Missed Information