@SellProduct @Smoke
Feature: UserSellProduct
  As a user, I want to sell a product in secondhand-store website

  @TC-WEB-SEL-006.001
  Scenario: User Sell The Product Accept Negotiation
    Then User click icons catalogs negotiation
    Then User click button diminati negotiation
    Then User choose product to sell negotiation
    Then User click button terima tawaran negotiation

  @TC-WEB-SEL-006.003
  Scenario: User Sell The Product Change Status
    Then User click icons catalogs change status 
    Then User click button diminati change status
    Then User choose product to sell change status
    Then User click button status
    Then User click form check status sold
    Then User click button kirim status    
    