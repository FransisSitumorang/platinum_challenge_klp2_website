<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>TambahProduk_Kategori</name>
   <tag></tag>
   <elementGuidId>58765063-8a37-4e98-97c3-56cbecabde8e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#kategori</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='kategori']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>57e75db8-7846-46bc-84cc-3cc9d2a72c5c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control</value>
      <webElementGuid>58da9ce4-ca3e-4710-be3c-c984e203c415</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>kategori</value>
      <webElementGuid>f00196e4-65f3-4636-846d-b89783fe238f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Pilih KategoriHobyKendaraanBajuElektronikKesehatan</value>
      <webElementGuid>5c96d1d9-5f39-4d93-9a83-8b86557d7403</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;kategori&quot;)</value>
      <webElementGuid>436be547-5f1e-4943-9b29-342521bcc1b7</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='kategori']</value>
      <webElementGuid>006c88d1-0c2f-431b-a9ce-c543d5b11acd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div/div/form/div[3]/select</value>
      <webElementGuid>4bae49d4-1258-45b6-8a8f-25fb5084a686</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kategori'])[1]/following::select[1]</value>
      <webElementGuid>095db832-8e3a-4434-8828-c91f36abffcd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Harga Produk'])[1]/following::select[1]</value>
      <webElementGuid>e809a72e-b9a2-4de7-ab21-6479fdd8487d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Deskripsi'])[1]/preceding::select[1]</value>
      <webElementGuid>60bdf084-e93e-4e68-b1eb-7adf18e6a15d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Foto Produk'])[1]/preceding::select[1]</value>
      <webElementGuid>8acd73b9-a9be-484e-b433-1a4282356bdf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//select</value>
      <webElementGuid>9d4aaa75-9bb5-4248-8ab9-e9838b743ac5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@id = 'kategori' and (text() = 'Pilih KategoriHobyKendaraanBajuElektronikKesehatan' or . = 'Pilih KategoriHobyKendaraanBajuElektronikKesehatan')]</value>
      <webElementGuid>62eb582a-e2af-4891-b662-f6dcee0afca2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
