package stepDefenition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class useraddproduct {

	@Then("User click Icon Catalogs Valid Data")
	public void user_click_Icon_Catalogs_Valid_Data() {
		WebUI.openBrowser(GlobalVariable.website)
		WebUI.click(findTestObject('Home_Page/Page_Secondhand Store/Home_Masuk'))
		WebUI.setText(findTestObject('Login/Page_Secondhand Store/Login_email'), GlobalVariable.email)
		WebUI.setText(findTestObject('Login/Page_Secondhand Store/Login_password'), GlobalVariable.password)
		WebUI.click(findTestObject('Login/Page_Secondhand Store/button_login'))
		WebUI.click(findTestObject('Tambah_Produk/Page_Secondhand Store/Icon-Catalog (Three dots)'))
	}

	@Then("User click Tambah Produk Valid Data")
	public void user_click_Tambah_Produk_Valid_Data() {
		WebUI.click(findTestObject('Daftar_Produk/Page_Secondhand Store/Tambah Produk'))
	}

	@Then("User input Nama Produk Valid Data")
	public void user_input_Nama_Produk_Valid_Data() {
		WebUI.setText(findTestObject('Tambah_Produk/Page_Secondhand Store/TambahProduk_Nama'), 'Fujifilm XA10')
	}

	@Then("User input Harga Produk Valid Data")
	public void user_input_Harga_Produk_Valid_Data() {
		WebUI.setText(findTestObject('Tambah_Produk/Page_Secondhand Store/TambahProduk_Harga'), '2650000')
	}

	@Then("User select option Kategori Valid Data")
	public void user_select_option_Kategori_Valid_Data() {
		WebUI.selectOptionByLabel(findTestObject('Tambah_Produk/Page_Secondhand Store/TambahProduk_Kategori'), 'Elektronik', false)
	}

	@Then("User input deskripsi Produk Valid Data")
	public void user_input_deskripsi_Produk_Valid_Data() {
		WebUI.setText(findTestObject('Tambah_Produk/Page_Secondhand Store/TambahProduk_Deskripsi'), 'Fuji film X-A10 (1359)')
	}

	@Then("User click button terbitkan Valid Data")
	public void user_click_button_terbitkan_Valid_Data() {
		WebUI.click(findTestObject('Tambah_Produk/Page_Secondhand Store/button_Terbitkan'))
	}

	@Then("User click Icon Catalogs Invalid Format")
	public void user_click_Icon_Catalogs_Invalid_Format() {
		WebUI.click(findTestObject('Tambah_Produk/Page_Secondhand Store/Icon-Catalog (Three dots)'))
	}

	@Then("User click Tambah Produk Invalid Format")
	public void user_click_Tambah_Produk_Invalid_Format() {
		WebUI.click(findTestObject('Daftar_Produk/Page_Secondhand Store/Tambah Produk'))
	}

	@Then("User input Nama Produk Invalid Format")
	public void user_input_Nama_Produk_Invalid_Format() {
		WebUI.setText(findTestObject('Tambah_Produk/Page_Secondhand Store/TambahProduk_Nama'), 'WKWKWK@$:)(')
	}

	@Then("User input Harga Produk Invalid Format")
	public void user_input_Harga_Produk_Invalid_Format() {
		WebUI.setText(findTestObject('Tambah_Produk/Page_Secondhand Store/TambahProduk_Harga'), '10000')
	}

	@Then("User select option Kategori Invalid Format")
	public void user_select_option_Kategori_Invalid_Format() {
		WebUI.selectOptionByLabel(findTestObject('Tambah_Produk/Page_Secondhand Store/TambahProduk_Kategori'), 'Kesehatan', false)
	}

	@Then("User input deskripsi Produk Invalid Format")
	public void user_input_deskripsi_Produk_Invalid_Format() {
		WebUI.setText(findTestObject('Tambah_Produk/Page_Secondhand Store/TambahProduk_Deskripsi'), 'Obat peredam keprihatinan')
	}

	@Then("User click button terbitkan Invalid Format")
	public void user_click_button_terbitkan_Invalid_Format() {
		WebUI.click(findTestObject('Tambah_Produk/Page_Secondhand Store/button_Terbitkan'))
	}

	@Then("User click Icon Catalogs Missed Information")
	public void user_click_Icon_Catalogs_Missed_Information() {
		WebUI.click(findTestObject('Tambah_Produk/Page_Secondhand Store/Icon-Catalog (Three dots)'))
	}

	@Then("User click Tambah Produk Missed Information")
	public void user_click_Tambah_Produk_Missed_Information() {
		WebUI.click(findTestObject('Daftar_Produk/Page_Secondhand Store/Tambah Produk'))
	}

	@Then("User input Nama Produk Missed Information")
	public void user_input_Nama_Produk_Missed_Information() {
		WebUI.setText(findTestObject('Tambah_Produk/Page_Secondhand Store/TambahProduk_Nama'), '')
	}

	@Then("User input Harga Produk Missed Information")
	public void user_input_Harga_Produk_Missed_Information() {
		WebUI.setText(findTestObject('Tambah_Produk/Page_Secondhand Store/TambahProduk_Harga'), '200000')
	}

	@Then("User select option Kategori Missed Information")
	public void user_select_option_Kategori_Missed_Information() {
		WebUI.selectOptionByLabel(findTestObject('Tambah_Produk/Page_Secondhand Store/TambahProduk_Kategori'), 'Kendaraan', false)
	}

	@Then("User input deskripsi Produk Missed Information")
	public void user_input_deskripsi_Produk_Missed_Information() {
		WebUI.setText(findTestObject('Tambah_Produk/Page_Secondhand Store/TambahProduk_Deskripsi'), 'Kendaraan Khayalan')
	}

	@Then("User click button terbitkan Missed Information")
	public void user_click_button_terbitkan_Missed_Information() {
		WebUI.click(findTestObject('Tambah_Produk/Page_Secondhand Store/button_Terbitkan'))
	}
}