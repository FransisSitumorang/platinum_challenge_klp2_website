<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>User want to register using incorrect data.
User want to register using duplicate data (registered account)
User want to register to the website with the correct data</description>
   <name>TS-WEB-REG-001</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>0ce34082-619e-48db-9539-360ff580f076</testSuiteGuid>
   <testCaseLink>
      <guid>b2c0b3f0-4dc3-4de7-9239-a97272c62130</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/UserRegistration/TC-WEB-REG-001.001</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>6657ed54-f7fb-488a-92f9-2eeafbb3e7c3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/UserRegistration/TC-WEB-REG-001.002</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>c6969015-3c66-4e8d-a6d9-1242bfc50653</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/UserRegistration/TC-WEB-REG-001.003</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
