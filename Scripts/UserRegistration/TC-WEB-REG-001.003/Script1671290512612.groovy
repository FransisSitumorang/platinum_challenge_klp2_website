import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.website)

WebUI.click(findTestObject('Home_Page/Page_Secondhand Store/Home_Masuk'))

WebUI.click(findTestObject('RegisterAkun/Page_Secondhand Store/Daftar_Akun'))

WebUI.setText(findTestObject('RegisterAkun/Page_Secondhand Store/Daftar_Nama'), 'Pirusi')

Date email = new Date()

String emailRegister = email.format('yyyyMMddHHmmss')

def email_code = ('aab' + emailRegister) + '@gmail.com'

WebUI.setText(findTestObject('RegisterAkun/Page_Secondhand Store/Daftar_Email'), email_code)

WebUI.setText(findTestObject('RegisterAkun/Page_Secondhand Store/Daftar_Password'), '12345678')

WebUI.click(findTestObject('RegisterAkun/Page_Secondhand Store/button_Daftar'))

WebUI.verifyElementText(findTestObject('RegisterAkun/Page_Secondhand Store/Register_silahkan_verifikasi_email'), 'Silahkan verifikasi email agar dapat menggunakan layanan kami')

