import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('Profiles/Page_Secondhand Store/button_profile'))

WebUI.click(findTestObject('Profiles/Page_Secondhand Store/Profiles'))

WebUI.setText(findTestObject('Profiles/Page_Secondhand Store/Profile_nama'), 'Cucezu')

WebUI.selectOptionByLabel(findTestObject('Profiles/Page_Secondhand Store/Profile_kota'), 'Bandung', false)

WebUI.setText(findTestObject('Profiles/Page_Secondhand Store/Profile_alamat'), 'Jl. Bandung Gg. Srirejeki No.99')

WebUI.setText(findTestObject('Profiles/Page_Secondhand Store/Profile_nohp'), '08136416452')

WebUI.click(findTestObject('Profiles/Page_Secondhand Store/profile_button_submit'))

WebUI.verifyElementText(findTestObject('Profiles/Page_Secondhand Store/UpdateProfile_Berhasil_updateprofile'), 'Berhasil update profile')

