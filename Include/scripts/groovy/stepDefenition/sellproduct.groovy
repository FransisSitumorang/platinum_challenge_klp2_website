package stepDefenition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class sellproduct {

	@Then("User click icons catalogs negotiation")
	public void user_click_icons_catalogs_negotiation() {
		WebUI.openBrowser(GlobalVariable.website)
		WebUI.click(findTestObject('Home_Page/Page_Secondhand Store/Home_Masuk'))
		WebUI.setText(findTestObject('Login/Page_Secondhand Store/Login_email'), GlobalVariable.email)
		WebUI.setText(findTestObject('Login/Page_Secondhand Store/Login_password'), GlobalVariable.password)
		WebUI.click(findTestObject('Login/Page_Secondhand Store/button_login'))
		WebUI.click(findTestObject('Tambah_Produk/Page_Secondhand Store/Icons Catalogs (three dots)'))
	}

	@Then("User click button diminati negotiation")
	public void user_click_button_diminati_negotiation() {
		WebUI.click(findTestObject('Daftar_Produk/Page_Secondhand Store/button_Diminati'))
	}

	@Then("User choose product to sell negotiation")
	public void user_choose_product_to_sell_negotiation() {
		WebUI.click(findTestObject('ProductDetails/Page_Secondhand Store/Product_toSell'), FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User click button terima tawaran negotiation")
	public void user_click_button_terima_tawaran_negotiation() {
		WebUI.click(findTestObject('ProductDetails/Page_Secondhand Store/button_Terima_Tawaran'))
	}

	@Then("User click icons catalogs change status")
	public void user_click_icons_catalogs_change_status() {
		WebUI.click(findTestObject('Tambah_Produk/Page_Secondhand Store/Icons Catalogs (three dots)'))
	}

	@Then("User click button diminati change status")
	public void user_click_button_diminati_change_status() {
		WebUI.click(findTestObject('Daftar_Produk/Page_Secondhand Store/button_Diminati'))
	}

	@Then("User choose product to sell change status")
	public void user_choose_product_to_sell_change_status() {
		WebUI.click(findTestObject('ProductDetails/Page_Secondhand Store/Product_toSell'), FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User click button status")
	public void user_click_button_status() {
		WebUI.click(findTestObject('ProductDetails/Page_Secondhand Store/button_Status_AfterTawaran'))
	}

	@Then("User click form check status sold")
	public void user_click_form_check_status_sold() {
		WebUI.click(findTestObject('ProductDetails/Page_Secondhand Store/Form_Check_Status_Sold'))
	}

	@Then("User click button kirim status")
	public void user_click_button_kirim_status() {
		WebUI.click(findTestObject('ProductDetails/Page_Secondhand Store/button_Kirim_Status'))
	}
}