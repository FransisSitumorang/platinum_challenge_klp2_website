@UserRegistration @Smoke
Feature: UserRegistration
  As a user, I want to register in secondhand-store website

  @TC-WEB-REG-001.001
  Scenario: Register Using Invalid Data
    Then User click on Home Masuk Invalid Data
    Then User click Daftar Akun Invalid Data
    Then User input nama Invalid Data
    Then User input email Invalid Data
    Then User input password Invalid Data
    Then User click button daftar using invalid data

  @TC-WEB-REG-001.002
  Scenario: Register Using Registered Account
    Then User click on Home Masuk Registered Account
    Then User click Daftar Akun Registered Account
    Then User input nama Registered Account
    Then User input email Registered Account
    Then User input password Registered Account
    Then User click button daftar using register account

  @TC-WEB-REG-001.003
 Scenario: Register Using Valid Data
    Then User click on Home Masuk Valid Data
    Then User click Daftar Akun Valid Data
    Then User input nama Valid Data
    Then User input email Valid Data
    Then User input password Valid Data
    Then User click button daftar using valid data