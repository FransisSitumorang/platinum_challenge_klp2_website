@UserLogin @Smoke
Feature: UserLogin
  As a user, I want to login to secondhand-store website

  @TC-WEB-LOG-002.001
  Scenario: Login Using Invalid Credentials
    Then User click on Home Masuk Invalid Credentials
    Then User input email Invalid Credentials
    Then User input password Invalid Credentials
    Then User click button login using invalid credentials
    Then Clear text email Invalid Credentials
    Then Clear Text Password Invalid Credentials

  @TC-WEB-LOG-002.002
  Scenario: Register Using Valid Credentials
    #Then User click on Home Masuk Valid Credentials
    Then User input email Valid Credentials
    Then User input password Valid Credentials
    Then User click button login using valid credentials
