@DeleteProduct @Smoke
Feature: UserDeleteProduct
  As a user, I want to delete a product in secondhand-store website

  @TC-WEB-DEL-007.001
  Scenario: User Delete The Product
    Then User click icons catalogs for delete
    Then User click button semua product
    Then User choose product to be deleted
    Then User click button hapus product

    