package stepDefenition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class userregistration {
	@Then("User click on Home Masuk Invalid Data")
	public void user_click_on_Home_Masuk_Invalid_Data() {
		WebUI.openBrowser(GlobalVariable.website)
		WebUI.click(findTestObject('Home_Page/Page_Secondhand Store/Home_Masuk'))
	}

	@Then("User click Daftar Akun Invalid Data")
	public void user_click_Daftar_Akun_Invalid_Data() {
		WebUI.click(findTestObject('RegisterAkun/Page_Secondhand Store/Daftar_Akun'))
	}

	@Then("User input nama Invalid Data")
	public void user_input_nama_Invalid_Data() {
		WebUI.setText(findTestObject('RegisterAkun/Page_Secondhand Store/Daftar_Nama'), 'FS$&:)')
	}

	@Then("User input email Invalid Data")
	public void user_input_email_Invalid_Data() {
		WebUI.setText(findTestObject('RegisterAkun/Page_Secondhand Store/Daftar_Email'), 'fs$&:)@comm.comm')
	}

	@Then("User input password Invalid Data")
	public void user_input_password_Invalid_Data() {
		WebUI.setText(findTestObject('RegisterAkun/Page_Secondhand Store/Daftar_Password'), 'bitino123')
	}

	@Then("User click on Home Masuk Registered Account")
	public void user_click_on_Home_Masuk_Registered_Account() {
		WebUI.click(findTestObject('Home_Page/Page_Secondhand Store/Home_Masuk'))
	}

	@Then("User click Daftar Akun Registered Account")
	public void user_click_Daftar_Akun_Registered_Account() {
		WebUI.click(findTestObject('RegisterAkun/Page_Secondhand Store/Daftar_Akun'))
	}

	@Then("User input nama Registered Account")
	public void user_input_nama_Registered_Account() {
		WebUI.setText(findTestObject('RegisterAkun/Page_Secondhand Store/Daftar_Nama'), 'Bocydever')
	}

	@Then("User input email Registered Account")
	public void user_input_email_Registered_Account() {
		WebUI.setText(findTestObject('RegisterAkun/Page_Secondhand Store/Daftar_Email'), 'bocydever@getnada.com')
	}

	@Then("User input password Registered Account")
	public void user_input_password_Registered_Account() {
		WebUI.setText(findTestObject('RegisterAkun/Page_Secondhand Store/Daftar_Password'), 'bocydever123')
	}

	@Then("User click on Home Masuk Valid Data")
	public void user_click_on_Home_Masuk_Valid_Data() {
		WebUI.click(findTestObject('Home_Page/Page_Secondhand Store/Home_Masuk'))
	}

	@Then("User click Daftar Akun Valid Data")
	public void user_click_Daftar_Akun_Valid_Data() {
		WebUI.click(findTestObject('RegisterAkun/Page_Secondhand Store/Daftar_Akun'))
	}

	@Then("User input nama Valid Data")
	public void user_input_nama_Valid_Data() {
		WebUI.setText(findTestObject('RegisterAkun/Page_Secondhand Store/Daftar_Nama'), 'Pirusi')
	}

	@Then("User input email Valid Data")
	public void user_input_email_Valid_Data() {
		WebUI.setText(findTestObject('RegisterAkun/Page_Secondhand Store/Daftar_Email'), 'tiqerydyx@getnada.com')
	}

	@Then("User input password Valid Data")
	public void user_input_password_Valid_Data() {
		WebUI.setText(findTestObject('RegisterAkun/Page_Secondhand Store/Daftar_Password'), 'tiqerydyx')
	}

	@Then("User click button daftar using invalid data")
	public void user_click_button_daftar_using_invalid_data() {
		WebUI.click(findTestObject('RegisterAkun/Page_Secondhand Store/button_Daftar'))
	}

	@Then("User click button daftar using register account")
	public void user_click_button_daftar_using_register_account() {
		WebUI.click(findTestObject('RegisterAkun/Page_Secondhand Store/button_Daftar'))
	}

	@Then("User click button daftar using valid data")
	public void user_click_button_daftar_using_valid_data() {
		WebUI.click(findTestObject('RegisterAkun/Page_Secondhand Store/button_Daftar'))
	}
}