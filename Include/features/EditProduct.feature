@UserEditProduct @Smoke
Feature: UserEditProduct
  As a user, I want to edit product in secondhand-store website

  @TC-WEB-PROD-004.001
  Scenario: User Edit Product Before Sold Out
    Then User click Icon Catalogs Product before sold out
    Then User choose the product before sold out
    Then User click edit button product before sold out
    Then User input new description for product before sold out
    Then User click button terbitkan for product before sold out
    
  @TC-WEB-PROD-004.001
  Scenario: User Edit Product After Sold Out
    Then User click Icon Catalogs Product after sold out
    Then User click button terjual product after sold out
    Then User choose product after sold out
    Then User verify element after sold out
