@UserUpdateProfiles @Smoke
Feature: UserUpdateProfiles
  As a user, I want to update profiles in secondhand-store website

  @TC-WEB-PROF-003.001
  Scenario: Update Profiles Using Invalid Data
    Then User click Button Profiles Invalid Data
    Then User click Profiles Invalid Data
    Then User input nama profiles Invalid Data
    Then User input kota Invalid Data
    Then User input alamat Invalid Data
    Then User input NoHP Invalid Data
    Then User click button submit profile using invalid data

  @TC-WEB-PROF-003.002
  Scenario: Update Profiles Using Valid Data
    Then User click Button Profiles Valid Data
    Then User click Profiles Valid Data
    Then User input nama profiles ValidData
    Then User input kota Valid Data
    Then User input alamat Valid Data
    Then User input NoHP Valid Data
    Then User click button submit profile using valid data

 