package stepDefenition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class userlogin {

	@Then("User click on Home Masuk Invalid Credentials")
	public void user_click_on_Home_Masuk_Invalid_Credentials() {
		WebUI.openBrowser(GlobalVariable.website)
		WebUI.click(findTestObject('Home_Page/Page_Secondhand Store/Home_Masuk'))
	}

	@Then("User input email Invalid Credentials")
	public void user_input_email_Invalid_Credentials() {
		WebUI.setText(findTestObject('Login/Page_Secondhand Store/Login_email'), 'wkwkwk@dudul.com')
	}

	@Then("User input password Invalid Credentials")
	public void user_input_password_Invalid_Credentials() {
		WebUI.setText(findTestObject('Login/Page_Secondhand Store/Login_password'), 'wkwk12')
	}

	@Then("User click button login using invalid credentials")
	public void user_click_button_login_using_invalid_credentials() {
		WebUI.click(findTestObject('Login/Page_Secondhand Store/button_login'))
	}

	@Then("Clear text email Invalid Credentials")
	public void clear_text_email_Invalid_Credentials() {
		WebUI.clearText(findTestObject('Login/Page_Secondhand Store/Login_email'))
	}

	@Then("Clear Text Password Invalid Credentials")
	public void clear_Text_Password_Invalid_Credentials() {
		WebUI.clearText(findTestObject('Login/Page_Secondhand Store/Login_password'))
	}

	//	@Then("User click on Home Masuk Valid Credentials")
	//	public void user_click_on_Home_Masuk_Valid_Credentials() {
	//		WebUI.click(findTestObject('Home_Page/Page_Secondhand Store/Home_Masuk'))
	//	}

	@Then("User input email Valid Credentials")
	public void user_input_email_Valid_Credentials() {
		WebUI.setText(findTestObject('Login/Page_Secondhand Store/Login_email'), 'cucezu@getnada.com')
	}

	@Then("User input password Valid Credentials")
	public void user_input_password_Valid_Credentials() {
		WebUI.setText(findTestObject('Login/Page_Secondhand Store/Login_password'), 'cucezu1234')
	}

	@Then("User click button login using valid credentials")
	public void user_click_button_login_using_valid_credentials() {
		WebUI.click(findTestObject('Login/Page_Secondhand Store/button_login'))
	}
}