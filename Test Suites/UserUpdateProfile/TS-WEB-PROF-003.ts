<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>User want to update the profile with the incorrect fomat
User want to update the profile with the correct fomat</description>
   <name>TS-WEB-PROF-003</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>e31f0b25-1b1f-4353-924f-95c74de0467f</testSuiteGuid>
   <testCaseLink>
      <guid>95eacbfd-92ff-4f77-ae5b-1a0fc99ff405</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/UserLogin/TC-WEB-LOG-002.002</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>ed8b4a6a-2f80-4ada-8ebb-c36492b9dcc3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/UserUpdateProfile/TC-WEB-PROF-003.001</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>5ce04b0e-f2e6-4ae3-b8a5-cfa6345868c1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/UserUpdateProfile/TC-WEB-PROF-003.002</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
