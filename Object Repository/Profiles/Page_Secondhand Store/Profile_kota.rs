<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Profile_kota</name>
   <tag></tag>
   <elementGuidId>10ebbf64-a30c-494e-9925-bb1b1cb96474</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='kota']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;kota&quot;)[count(. | //*[@id = 'kota' and (text() = 'Pilih kotaBandungBogorJemberKediriLumajangMalangPasuruanProbolinggoYogyakarta' or . = 'Pilih kotaBandungBogorJemberKediriLumajangMalangPasuruanProbolinggoYogyakarta')]) = count(//*[@id = 'kota' and (text() = 'Pilih kotaBandungBogorJemberKediriLumajangMalangPasuruanProbolinggoYogyakarta' or . = 'Pilih kotaBandungBogorJemberKediriLumajangMalangPasuruanProbolinggoYogyakarta')])]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#kota</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>bc87e807-3609-4fd0-820d-8b58788acec0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control</value>
      <webElementGuid>43bb57ef-c978-4896-8493-7fc8313f26ee</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>kota</value>
      <webElementGuid>60fd1598-13c8-442a-b08e-e58675ef54f9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Pilih kotaBandungBogorJemberKediriLumajangMalangPasuruanProbolinggoYogyakarta</value>
      <webElementGuid>e7621a9d-f8aa-4b18-adab-679297ae2cce</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;kota&quot;)</value>
      <webElementGuid>8f5e7a9e-f20b-489c-a283-a3fe3b0850fe</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='kota']</value>
      <webElementGuid>3a4e5919-875c-4802-9473-7dbc676ce73d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div/div[2]/form/div[2]/select</value>
      <webElementGuid>9d2de6c2-b316-4991-ac82-1573dfa133ae</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kota*'])[1]/following::select[1]</value>
      <webElementGuid>c4d48c04-08b8-40a8-93b1-dfae8d7fafce</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Nama*'])[1]/following::select[1]</value>
      <webElementGuid>715b50c1-e0e8-4e98-b632-1f8f68396b0d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Alamat*'])[1]/preceding::select[1]</value>
      <webElementGuid>80158eea-19dd-4f88-ba27-f5bb706ca5c3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='No.Handphone*'])[1]/preceding::select[1]</value>
      <webElementGuid>c13218c7-8101-415a-a3da-f8a1fd200961</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//select</value>
      <webElementGuid>be6313d1-651d-47f2-bab7-e866e8d7c3d7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@id = 'kota' and (text() = 'Pilih kotaBandungBogorJemberKediriLumajangMalangPasuruanProbolinggoYogyakarta' or . = 'Pilih kotaBandungBogorJemberKediriLumajangMalangPasuruanProbolinggoYogyakarta')]</value>
      <webElementGuid>10ab8372-f030-4753-85ba-1a7dc0a99ffc</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
