<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>Delete product that was added in user catalog</description>
   <name>TC-WEB-DEL-007</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>d542a8c6-672d-499c-88f7-af44a30aeaad</testSuiteGuid>
   <testCaseLink>
      <guid>68a7629d-74ed-4c8d-b819-4a32bb55fe7b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/UserLogin/TC-WEB-LOG-002.002</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>bc541268-3e5e-414e-b5ec-d873740f1715</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DeleteProduct/TC-WEB-DEL-007.001</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
