package stepDefenition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class editproduct {

	@Then("User click Icon Catalogs Product before sold out")
	public void user_click_Icon_Catalogs_Product_before_sold_out() {
		WebUI.openBrowser(GlobalVariable.website)
		WebUI.click(findTestObject('Home_Page/Page_Secondhand Store/Home_Masuk'))
		WebUI.setText(findTestObject('Login/Page_Secondhand Store/Login_email'), GlobalVariable.email)
		WebUI.setText(findTestObject('Login/Page_Secondhand Store/Login_password'), GlobalVariable.password)
		WebUI.click(findTestObject('Login/Page_Secondhand Store/button_login'))
		WebUI.click(findTestObject('Tambah_Produk/Page_Secondhand Store/Icons Catalogs (three dots)'))
	}

	@Then("User choose the product before sold out")
	public void user_choose_the_product_before_sold_out() {
		WebUI.click(findTestObject('EditProduct/Page_Secondhand Store/Pilih_Product_to_edit'))
	}

	@Then("User click edit button product before sold out")
	public void user_click_edit_button_product_before_sold_out() {
		WebUI.click(findTestObject('EditProduct/Page_Secondhand Store/button_Edit_Product'))
	}

	@Then("User input new description for product before sold out")
	public void user_input_new_description_for_product_before_sold_out() {
		WebUI.setText(findTestObject('Tambah_Produk/Page_Secondhand Store/TambahProduk_Deskripsi'), 'AirPods Gen 3 yang canggih')
	}

	@Then("User click button terbitkan for product before sold out")
	public void user_click_button_terbitkan_for_product_before_sold_out() {
		WebUI.click(findTestObject('Tambah_Produk/Page_Secondhand Store/button_Terbitkan'))
	}

	@Then("User click Icon Catalogs Product after sold out")
	public void user_click_Icon_Catalogs_Product_after_sold_out() {
		WebUI.click(findTestObject('Tambah_Produk/Page_Secondhand Store/Icons Catalogs (three dots)'))
	}

	@Then("User click button terjual product after sold out")
	public void user_click_button_terjual_product_after_sold_out() {
		WebUI.click(findTestObject('Daftar_Produk/Page_Secondhand Store/button_Terjual'))
	}

	@Then("User choose product after sold out")
	public void user_choose_product_after_sold_out() {
		WebUI.click(findTestObject('EditProduct/Page_Secondhand Store/Pilih_Product_to_edit'))
	}

	@Then("User verify element after sold out")
	public void user_verify_element_after_sold_out() {
		WebUI.verifyElementText(findTestObject('EditProduct/Page_Secondhand Store/Produkmu sudah terjual'), 'Produkmu sudah terjual')
	}
}