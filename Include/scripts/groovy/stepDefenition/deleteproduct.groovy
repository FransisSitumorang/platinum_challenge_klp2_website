package stepDefenition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class deleteproduct {

	@Then("User click icons catalogs for delete")
	public void user_click_icons_catalogs_for_delete() {
		WebUI.openBrowser(GlobalVariable.website)
		WebUI.click(findTestObject('Home_Page/Page_Secondhand Store/Home_Masuk'))
		WebUI.setText(findTestObject('Login/Page_Secondhand Store/Login_email'), GlobalVariable.email)
		WebUI.setText(findTestObject('Login/Page_Secondhand Store/Login_password'), GlobalVariable.password)
		WebUI.click(findTestObject('Login/Page_Secondhand Store/button_login'))
		WebUI.click(findTestObject('Tambah_Produk/Page_Secondhand Store/Icons Catalogs (three dots)'))
	}

	@Then("User click button semua product")
	public void user_click_button_semua_product() {
		WebUI.click(findTestObject('Daftar_Produk/Page_Secondhand Store/button_Semua Product'))
	}

	@Then("User choose product to be deleted")
	public void user_choose_product_to_be_deleted() {
		WebUI.click(findTestObject('EditProduct/Page_Secondhand Store/Pilih_Product_to_delete'))
	}

	@Then("User click button hapus product")
	public void user_click_button_hapus_product() {
		WebUI.click(findTestObject('EditProduct/Page_Secondhand Store/button_Hapus_Product'))
	}
}