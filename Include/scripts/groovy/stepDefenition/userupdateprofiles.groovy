package stepDefenition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class userupdateprofiles {

	@Then("User click Button Profiles Invalid Data")
	public void user_click_Button_Profiles_Invalid_Data() {
		WebUI.openBrowser(GlobalVariable.website)
		WebUI.click(findTestObject('Home_Page/Page_Secondhand Store/Home_Masuk'))
		WebUI.setText(findTestObject('Login/Page_Secondhand Store/Login_email'), GlobalVariable.email)
		WebUI.setText(findTestObject('Login/Page_Secondhand Store/Login_password'), GlobalVariable.password)
		WebUI.click(findTestObject('Login/Page_Secondhand Store/button_login'))
		WebUI.click(findTestObject('Profiles/Page_Secondhand Store/button_profile'))
	}
	@Then("User click Profiles Invalid Data")
	public void user_click_Profiles_Invalid_Data() {
		WebUI.click(findTestObject('Profiles/Page_Secondhand Store/Profiles'))
	}

	@Then("User input nama profiles Invalid Data")
	public void user_input_nama_profiles_Invalid_Data() {
		WebUI.setText(findTestObject('Profiles/Page_Secondhand Store/Profile_nama'), 'Bitinof%$^')
	}

	@Then("User input kota Invalid Data")
	public void user_input_kota_Invalid_Data() {
		WebUI.selectOptionByLabel(findTestObject('Profiles/Page_Secondhand Store/Profile_kota'), 'Bogor', false)
	}

	@Then("User input alamat Invalid Data")
	public void user_input_alamat_Invalid_Data() {
		WebUI.setText(findTestObject('Profiles/Page_Secondhand Store/Profile_alamat'), 'Kokas Details431')
	}

	@Then("User input NoHP Invalid Data")
	public void user_input_NoHP_Invalid_Data() {
		WebUI.setText(findTestObject('Profiles/Page_Secondhand Store/Profile_nohp'), 'LOL1234$')
	}

	@Then("User click button submit profile using invalid data")
	public void user_click_button_submit_profile_using_invalid_data() {
		WebUI.click(findTestObject('Profiles/Page_Secondhand Store/profile_button_submit'))
	}


	@Then("User click Button Profiles Valid Data")
	public void user_click_Button_Profiles_Valid_Data() {
		WebUI.openBrowser(GlobalVariable.website)
		WebUI.click(findTestObject('Home_Page/Page_Secondhand Store/Home_Masuk'))
		WebUI.setText(findTestObject('Login/Page_Secondhand Store/Login_email'), GlobalVariable.email)
		WebUI.setText(findTestObject('Login/Page_Secondhand Store/Login_password'), GlobalVariable.password)
		WebUI.click(findTestObject('Login/Page_Secondhand Store/button_login'))
		WebUI.click(findTestObject('Profiles/Page_Secondhand Store/button_profile'))
	}

	@Then("User click Profiles Valid Data")
	public void user_click_Profiles_Valid_Data() {
		WebUI.click(findTestObject('Profiles/Page_Secondhand Store/Profiles'))
	}

	@Then("User input nama profiles ValidData")
	public void user_input_nama_profiles_ValidData() {
		WebUI.setText(findTestObject('Profiles/Page_Secondhand Store/Profile_nama'), 'Bitino')
	}

	@Then("User input kota Valid Data")
	public void user_input_kota_Valid_Data() {
		WebUI.selectOptionByLabel(findTestObject('Profiles/Page_Secondhand Store/Profile_kota'), 'Bandung', false)
	}

	@Then("User input alamat Valid Data")
	public void user_input_alamat_Valid_Data() {
		WebUI.setText(findTestObject('Profiles/Page_Secondhand Store/Profile_alamat'), 'Jl. Bandung Gg. Srirejeki No.99')
	}

	@Then("User input NoHP Valid Data")
	public void user_input_NoHP_Valid_Data() {
		WebUI.setText(findTestObject('Profiles/Page_Secondhand Store/Profile_nohp'), '08136416452')
	}

	@Then("User click button submit profile using valid data")
	public void user_click_button_submit_profile_using_valid_data() {
		WebUI.click(findTestObject('Profiles/Page_Secondhand Store/profile_button_submit'))
	}
}